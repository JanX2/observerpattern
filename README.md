#ObserverPattern

This project demonstrates an Objective-C Key-Value Observing system. Observation is performed by block objects, providing for generic callbacks rather than a funnel observer method. Observation occurs asynchronously. Rather than use context pointers, observers are handed tokens which they can use to relinquish their observation.

Of course, those are just the upsides, here's a list of current limitations (pull requests are always welcome):

 - Unlike Cocoa KVO, this implementation does not currently support key paths. Whether you consider this a preservation of the Law of Demeter or a deal-breaking failure, you can only observe properties on the object to which you add the observer.
 - There is not currently any way to choose a particular context (thread, queue etc) in which observation blocks are executed.
 
## Licence

MIT. See COPYING.